# Base module 
import os
import sys

# Structure
import pandas as pd
import numpy as np

# DEEPL module
from keras.layers import Dense, Input, Embedding,concatenate,Bidirectional,LSTM, Dropout
from keras.models import Model
from keras.callbacks import ModelCheckpoint

# Custom module
from lib.utils_geo import zero_one_encoding
from lib.ngram_index import NgramIndex
from lib.word_index import WordIndex
from lib.utils import ConfigurationReader
from lib.utils_geo import accuracy_k,haversine_tf_1circle
from helpers import EpochTimer
from lib.datageneratorv4 import DataGenerator

# Logging
import logging
logging.getLogger('gensim').setLevel(logging.WARNING)
logging.basicConfig( # LOGGING CONF
    format='[%(asctime)s][%(levelname)s] %(message)s ', 
    datefmt='%m/%d/%Y %I:%M:%S %p',
    level=logging.INFO  
    )

import tensorflow as tf
try:
    physical_devices = tf.config.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(physical_devices[0], enable=True)
except:
    print("NO GPU FOUND...")

# COMMAND ARGS
args = ConfigurationReader("./parser_config/toponym_combination_embedding_v3.json")\
    .parse_args()#("IGN ../data/IGN/IGN_inclusion.csv ../data/IGN/IGN_adjacent_corrected.csv ../data/IGN/IGN_cooc.csv -i -w  -a -n 4 --ngram-word2vec-iter 1".split())

#
#################################################
############# MODEL TRAINING PARAMETER ##########
#################################################
MODEL_NAME = "Bi-LSTM_NGRAM"
NGRAM_SIZE = args.ngram_size
ACCURACY_TOLERANCE = args.tolerance_value
EPOCHS = args.epochs
WORDVEC_ITER = args.ngram_word2vec_iter
EMBEDDING_DIM = args.dimension
#################################################
########## FILENAME VARIABLE ####################
#################################################
INCLUSION_FN = args.geoname_inclusion
ADJACENT_FN = args.geonames_adjacent
COOC_FN = args.wikipedia_cooc

DATASET_NAME = args.dataset_name

PREFIX_OUTPUT_FN = DATASET_NAME
PREFIX_OUTPUT_FN+="_{0}".format(NGRAM_SIZE)
PREFIX_OUTPUT_FN+="_{0}".format(EPOCHS)

if args.adjacency:
    PREFIX_OUTPUT_FN += "_A"
if args.inclusion:
    PREFIX_OUTPUT_FN += "_I"
if args.wikipedia:
    PREFIX_OUTPUT_FN += "_P"

MODEL_OUTPUT_FN = "outputs/{0}.h5".format(PREFIX_OUTPUT_FN)
INDEX_FN = "outputs/{0}_index".format(PREFIX_OUTPUT_FN)
HISTORY_FN = "outputs/{0}.csv".format(PREFIX_OUTPUT_FN)

#############################################################################################
################################# LOAD DATA #################################################
#############################################################################################

data_used = []

if args.wikipedia:
    data_used.append(pd.read_csv(COOC_FN,sep="\t"))

if args.inclusion:
    data_used.append(pd.read_csv(INCLUSION_FN,sep="\t"))

if args.adjacency:
    data_used.append(pd.read_csv(ADJACENT_FN, sep="\t"))

if  len(data_used) <1:
    print("No Type of toponyms indicated. Stopping the program...")
    sys.exit(1)

pairs_of_toponym = pd.concat(data_used)

#############################################################################################
################################# RETRIEVE RELATIONSHIPS ####################################
#############################################################################################

# ENCODING NAME USING N-GRAM SPLITTING
logging.info("Encoding toponyms to ngram...")
index = NgramIndex(NGRAM_SIZE)
if args.tokenization_method == "word-level":
    index = WordIndex()
if args.tokenization_method == "bert":
    index = NgramIndex(NGRAM_SIZE,bert_tokenization=True)

 # Identify all ngram available
pairs_of_toponym.toponym.apply(lambda x : index.split_and_add(x))
pairs_of_toponym.toponym_context.apply(lambda x : index.split_and_add(x))

num_words = len(index.index_ngram) # necessary for the embedding matrix

# SAVE THE INDEX TO REUSE THE MODEL
index.save(INDEX_FN)
logging.info("Done !")

#############################################################################################
################################# NGRAM EMBEDDINGS ##########################################
#############################################################################################

logging.info("Generating N-GRAM Embedding...")
embedding_weights = index.get_embedding_layer([index.encode(p) for p in np.concatenate((pairs_of_toponym.toponym.unique(),pairs_of_toponym.toponym_context.unique()))],dim= EMBEDDING_DIM,iter=WORDVEC_ITER)
logging.info("Embedding generated !")

#############################################################################################
################################# BUILD TRAIN/TEST DATASETS #################################
#############################################################################################
logging.info("Preparing Input and Output data...")

training_generator = DataGenerator(pairs_of_toponym[pairs_of_toponym.split == "train"],index)
validation_generator = DataGenerator(pairs_of_toponym[pairs_of_toponym.split == "test"],index)
# X_1_train,X_2_train=[],[]
# X_1_test,X_2_test=[],[]
# y_train,y_test = [],[]

# for couple in pairs_of_toponym["toponym toponym_context split longitude latitude".split()].itertuples():
#     top,top_c,split_ = couple[1], couple[2], couple[3]
#     coord = zero_one_encoding(couple[-2],couple[-1]) # 0 and 1 encoding
#     enc_top, enc_top_c = index.encode(top),index.encode(top_c)
#     if split_ == "train":
#         X_1_train.append(enc_top)
#         X_2_train.append(enc_top_c)
#         y_train.append(list(coord))
#     else:
#         X_1_test.append(enc_top)
#         X_2_test.append(enc_top_c)
#         y_test.append(list(coord))

# # "NUMPYZE" inputs and output lists
# X_1_train = np.array(X_1_train)
# X_2_train = np.array(X_2_train)
# y_train = np.array(y_train)

# X_1_test = np.array(X_1_test)
# X_2_test = np.array(X_2_test)
# y_test = np.array(y_test)

logging.info("Data prepared !")


# check for output dir
if not os.path.exists("outputs/"):
    os.makedirs("outputs/")


#############################################################################################
################################# MODEL DEFINITION ##########################################
#############################################################################################

input_1 = Input(shape=(index.max_len,))
input_2 = Input(shape=(index.max_len,))

embedding_layer = Embedding(num_words, EMBEDDING_DIM,input_length=index.max_len,weights=[embedding_weights],trainable=False)#, trainable=True)

x1 = embedding_layer(input_1)
x2 = embedding_layer(input_2)

# Each LSTM learn on a permutation of the input toponyms
if args.lstm_layer == 2:
    x1 = Bidirectional(LSTM(100))(x1)
    x2 = Bidirectional(LSTM(100))(x2)
    x = concatenate([x1,x2])
else:
    lstm_unique_layer = Bidirectional(LSTM(100))
    x1 = lstm_unique_layer(x1)
    x2 = lstm_unique_layer(x2)
    x = concatenate([x1,x2])

x1 = Dense(500,activation="relu")(x)
x1 = Dense(500,activation="relu")(x1)

x2 = Dense(500,activation="relu")(x)
x2 = Dense(500,activation="relu")(x2)

output_lon = Dense(1,activation="sigmoid",name="Output_LON")(x1)
output_lat = Dense(1,activation="sigmoid",name="Output_LAT")(x2)

output_coord = concatenate([output_lon,output_lat],name="output_coord")

model = Model(inputs = [input_1,input_2], outputs = output_coord)#input_3
model.compile(loss={"output_coord":haversine_tf_1circle}, optimizer='adam',metrics={"output_coord":accuracy_k(ACCURACY_TOLERANCE)})

print("Neural Network Architecture : ")
print(model.summary())
#############################################################################################
################################# TRAINING LAUNCH ###########################################
#############################################################################################

checkpoint = ModelCheckpoint(MODEL_OUTPUT_FN + ".part", monitor='loss', verbose=1,
    save_best_only=True, mode='auto', period=1)

epoch_timer = EpochTimer("outputs/"+PREFIX_OUTPUT_FN+"_epoch_timer_output.csv")



history = model.fit(training_generator,verbose=True,
                    validation_data=validation_generator,
                    callbacks=[checkpoint,epoch_timer],epochs=EPOCHS)

# history = model.fit(x=[X_1_train,X_2_train],
#     y=y_train,
#     verbose=True, batch_size=100,
#     epochs=EPOCHS,
#     validation_data=([X_1_test,X_2_test],y_test),#[y_lon_test,y_lat_test]),
#     callbacks=[checkpoint,epoch_timer])


hist_df = pd.DataFrame(history.history)
hist_df.to_csv(HISTORY_FN)

model.save(MODEL_OUTPUT_FN)

# Erase Model Checkpoint file
if os.path.exists(MODEL_OUTPUT_FN + ".part"):
    try:
        import shutil
        shutil.rmtree(MODEL_OUTPUT_FN + ".part")
    except: # Depends on Keras version
        os.remove(MODEL_OUTPUT_FN + ".part")