import argparse

import pandas as pd
import numpy as np
import geopandas as gpd

import logging
logging.basicConfig(
    format='[%(asctime)s][%(levelname)s] %(message)s ', 
    datefmt='%m/%d/%Y %I:%M:%S %p',
    level=logging.INFO
    )

from sklearn.model_selection import train_test_split
from shapely.geometry import Point

from lib.utils_geo import latlon2healpix

from tqdm import tqdm 

parser = argparse.ArgumentParser()
parser.add_argument("cooccurrence_file")
parser.add_argument("-s",action="store_true")

args = parser.parse_args()#("data/wikipedia/cooccurrence_FR.txt".split())#("data/geonamesData/FR.txt".split())

# LOAD DATAgeopandas
COOC_FN = args.cooccurrence_file

logging.info("Load Cooc DATA data...")
cooc_data = pd.read_csv(COOC_FN,sep="\t").fillna("")
logging.info("Cooc data loaded!")


cooc_data["cat"] = cooc_data.apply(lambda x:latlon2healpix(x.latitude,x.longitude,64),axis=1)

# TRAIN AND TEST SPLIT
logging.info("Split Between Train and Test")

#  Cell can be empty
i=0
while 1:
    if len(cooc_data[cooc_data.cat == i])> 1:
        X_train,X_test = train_test_split(cooc_data[cooc_data.cat == i])
        break
    i+=1

for i in np.unique(cooc_data.cat.values):
    try:
        if not args.s:
            x_train,x_test = train_test_split(cooc_data[cooc_data.cat == i])
        else:
            x_train,x_test = train_test_split(cooc_data[cooc_data.cat == i].sample(frac=0.1))

        X_train,X_test = pd.concat((X_train,x_train)),pd.concat((X_test,x_test))
    except Exception as e:
        print(e) #print("Error",len(filtered[filtered.cat == i]))

del X_train["cat"]
del X_test["cat"]

# SAVING THE DATA
logging.info("Saving Output !")
suffix =""
if args.s:
    suffix = "10per"
X_train.to_csv(COOC_FN+suffix+"_train.csv")
X_test.to_csv(COOC_FN+suffix+"_test.csv")
