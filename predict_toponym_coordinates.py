from keras.models import load_model
import os
import tensorflow as tf
import keras.backend as K
from lib.ngram_index import NgramIndex
from lib.word_index import WordIndex
import numpy as np

from tensorflow.python.keras.backend import set_session
from tensorflow.python.keras.models import load_model


from lib.utils_geo import haversine_tf_1circle
sess = None
graph = None

def lat_accuracy(LAT_TOL =1/180.):
    def accuracy_at_k_lat(y_true, y_pred):
        """
        Metrics use to measure the accuracy of the coordinate prediction. But in comparison to the normal accuracy metrics, we add a tolerance threshold due to the (quasi) impossible 
        task for neural network to obtain the exact  coordinate.

        Parameters
        ----------
        y_true : tf.Tensor
            truth data
        y_pred : tf.Tensor
            predicted output
        """
        diff = tf.abs(y_true - y_pred)
        fit = tf.dtypes.cast(tf.less(diff,LAT_TOL),tf.int64)
        return tf.reduce_sum(fit)/tf.size(y_pred,out_type=tf.dtypes.int64)
    return accuracy_at_k_lat

def lon_accuracy(LON_TOL=1/360.):
    def accuracy_at_k_lon(y_true, y_pred):
        """
        Metrics use to measure the accuracy of the coordinate prediction. But in comparison to the normal accuracy metrics, we add a tolerance threshold due to the (quasi) impossible 
        task for neural network to obtain the exact  coordinate.

        Parameters
        ----------
        y_true : tf.Tensor
            truth data
        y_pred : tf.Tensor
            predicted output
        """
        diff = tf.abs(y_true - y_pred)
        fit = tf.dtypes.cast(tf.less(diff,LON_TOL),tf.int64)
        return tf.reduce_sum(fit)/tf.size(y_pred,out_type=tf.dtypes.int64)
    return accuracy_at_k_lon

class Geocoder(object):
    """
    >>>geocoder = Geocoder("LSTM_FR.txt_20_4_0.002_None_A_I_C.h5","index_4gram_FR_backup.txt")
    >>>lon,lat = geocoder.get_coord("Paris","New-York")
    >>>lon,lat = geocoder.wgs_coord(lon,lat)
    >>>geocoder.plot_coord("Paris,New-York",lat,lon)

    if you want an interactive map using leafletJS, set to True the `interactive_map` parameter of `Geocoder.plot_coord()`
    """
    def __init__(self,keras_model_fn,ngram_index_file):
        # global sess
        # global graph
        # sess = tf.compat.v1.Session()
        # graph = tf.compat.v1.get_default_graph()
        # set_session(sess)
        self.keras_model = load_model(keras_model_fn,custom_objects={"loss":haversine_tf_1circle},compile=False)#custom_objects={"accuracy_at_k_lat":lat_accuracy(),"accuracy_at_k_lon":lon_accuracy()})
        self.ngram_encoder = NgramIndex.load(ngram_index_file)

    def get_coord(self,toponym,context_toponym):
        global sess
        global graph
        p = self.ngram_encoder.complete(self.ngram_encoder.encode(toponym),self.ngram_encoder.max_len)
        c = self.ngram_encoder.complete(self.ngram_encoder.encode(context_toponym),self.ngram_encoder.max_len)
        p = np.array(p)
        c = np.array(c)       
        # with sess.as_default():
        #     with graph.as_default():
        coord = self.keras_model.predict([[p],[c]])
        return coord[0][0],coord[0][1]
    
    def get_coords(self,list_toponym,list_toponym_context):
        p = [self.ngram_encoder.complete(self.ngram_encoder.encode(toponym),self.ngram_encoder.max_len) for toponym in list_toponym]
        c = [self.ngram_encoder.complete(self.ngram_encoder.encode(toponym),self.ngram_encoder.max_len) for toponym in list_toponym_context]

        p = np.array(p)
        c = np.array(c)
        
        coords = self.keras_model.predict([p,c])
        return coords[0],coords[1]

    def wgs_coord(self,lon,lat):
        return ((lon*360)-180),((lat*180)-90)
    
    def plot_coord(self,toponym,lat,lon,interactive_map=False,**kwargs):
        if interactive_map:
            import folium
            import tempfile
            import webbrowser
            fp = tempfile.NamedTemporaryFile(delete=False)
            m = folium.Map()
            folium.Marker([lat, lon], popup=toponym).add_to(m)
            m.save(fp.name)
            webbrowser.open('file://' + fp.name)
        else:
            import matplotlib.pyplot as plt
            import geopandas
            fig, ax = plt.subplots(1,**kwargs)
            world = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))
            world.plot(color='white', edgecolor='black',ax=ax)
            ax.plot(lon,lat,marker='o', color='red', markersize=5)
            plt.show()



if __name__ == "__main__":
    from flask import Flask, escape, request, render_template

    app = Flask(__name__)


    geocoder = Geocoder("outputs/LSTM_FR.txt_100_4_0.002_None_A_I_C.h5","./outputs/FR.txt_100_4_0.002_None_A_I_C_index")

    @app.route('/',methods=["GET"])
    def display():
        toponym = request.args.get("top", "Paris")
        c_toponym = request.args.get("c_top", "Cherbourg")
        lon,lat = geocoder.get_coord(toponym,c_toponym)
        lon,lat = geocoder.wgs_coord(lon,lat)
        return  render_template("skeleton.html",lat=lat,lon=lon)

    app.run(host='0.0.0.0')