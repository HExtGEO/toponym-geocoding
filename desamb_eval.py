from glob import glob
import json

import argparse
import logging

import pandas as pd



parser = argparse.ArgumentParser()
parser.add_argument("eval_dataset")
parser.add_argument("models_directory")
parser.add_argument("-g","--gpu",action="store_true")
args = parser.parse_args()#("-g ../data/geocoding_evaluation/fr_cooc_test.csv outputs/FR_RESULT".split())


if not args.gpu:
    import os
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1' # No need for GPU


from predict_toponym_coordinates import Geocoder
from lib.utils_geo import haversine_pd

logging.getLogger("tensorflow").setLevel(logging.CRITICAL)
logging.getLogger("tensorflow_hub").setLevel(logging.CRITICAL)

EVAL_DATASET_FN= args.eval_dataset#"./test_dataset_ambiguity.csv"


def eval_model(eval_dataset_fn,model_fn,model_index_fn):
    print("Dataset -- {0} -- Model -- {1}".format(\
        eval_dataset_fn.split("/")[-1],
        model_fn.split("/")[-1]))
    df = pd.read_csv(eval_dataset_fn)
    geocoder = Geocoder(model_fn,model_index_fn)
    lon,lat = geocoder.get_coords(df.name1.values,df.name2.values)                  
    lon,lat = geocoder.wgs_coord(lon,lat)

    df["p_longitude"] = lon
    df["p_latitude"] = lat

    df["dist"] = haversine_pd(df.longitude,df.latitude,df.p_longitude,df.p_latitude)

    print("100km",(df.dist<100).sum()/len(df))
    print("50km",(df.dist<50).sum()/len(df))
    print("20km",(df.dist<20).sum()/len(df))
    return df

prefixes = [x.rstrip(".h5") for x in glob(args.models_directory+"/*.h5")]

final_output = []
for prefix in prefixes:
    try:
        df = eval_model(EVAL_DATASET_FN,prefix + ".h5",prefix + "_index")
        data = json.load(open(prefix+".json"))
        data["acccuracy@100km"] = (df.dist<100).sum()/len(df)
        data["acccuracy@50km"] = (df.dist<50).sum()/len(df)
        data["acccuracy@25km"] = (df.dist<25).sum()/len(df)
        final_output.append(data)
    except:
        print("BUMP!")
    

pd.DataFrame(final_output).to_csv("{0}_RESULT.csv".format(EVAL_DATASET_FN.rstrip(".csv")))