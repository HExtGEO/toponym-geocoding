import os
from gzip import GzipFile

import keras
from keras.utils import to_categorical
import numpy as np
import pandas as pd

from .utils_geo import zero_one_encoding

from helpers import parse_title_wiki,read_geonames
from gensim.models.keyedvectors import KeyedVectors

from sklearn.preprocessing import LabelEncoder


def wc_l(filename,gzip=True):
    lc = 0
    if not gzip:
        f = open(filename)
    if gzip:
        f = GzipFile(filename)
    while f.readline():
        lc += 1 
    f.close()       
    return lc

class SamplingProbabilities:
    def __init__(self):
        self.count = {}
    
    def get_probs(self,item):
        if not item in self.count:
            self.count[item] = 0
        self.count[item]+=1
        return 1/self.count[item]
    def __call__(self,a):
        return self.get_probs(a)
        

class DataSource(object):
    def __init__(self,name,input_filename):
        self.name = name
        assert os.path.exists(input_filename)
        self.input_filename = input_filename
        self.len = 0

        self.is_there_healpix = False

    def __next__(self):
        raise NotImplementedError()

    def __iter__(self):
        return self
    
    def __len__(self):
        return self.len
    
    def __reset__(self):
        raise NotImplementedError()

    def isOver(self):
        raise NotImplementedError()

class Adjacency(DataSource):
    def __init__(self,filename,geonames_filename,sampling=3,len_=None,gzip=True):
        DataSource.__init__(self,"Adjacency SRC",filename)

        assert os.path.exists(geonames_filename)
        self.geonames_data_dict = {row.geonameid:row.name for row in read_geonames(geonames_filename).itertuples()}
        
        self.gzip = gzip
        if not self.gzip:
            self.data_src = open(self.input_filename,'rb')
        else:
            self.data_src = GzipFile(self.input_filename,'rb')

        if len_:
            self.len = len_*sampling
        else:
            self.len = wc_l(filename,gzip=gzip)
        
        self.data_src.readline() # header line

        self.sampling = sampling
        if self.sampling:
            self.probs_storage = SamplingProbabilities() 

        self.topo = None
        self.context_topo_context = []
        self.curr_probs = None
        self.lat, self.lon = None, None


        self.i = 0
        self.is_over = False
    
    def __next__(self):
        if  self.i >= len(self.context_topo_context):
            line = self.data_src.readline()
            if not line:
                self.is_over = True
                raise StopIteration
            line = line.decode("utf-8").rstrip("\n")
            _,geonameid, adjacent_geoname_id,latitude,longitude = tuple(line.split(","))

            self.topo = int(geonameid)
            self.context_topo_context = [int(x) for x in adjacent_geoname_id.split("|")]
            if self.sampling:
                self.curr_probs = [self.probs_storage(x) for x in self.context_topo_context]
                self.context_topo_context = np.random.choice(self.context_topo_context,self.sampling,self.curr_probs)
            self.lat, self.lon = float(latitude),float(longitude)

            self.i = 0
        
        self.i += 1
        return (self.geonames_data_dict[self.topo],
        self.geonames_data_dict[self.context_topo_context[self.i-1]],
        self.lat,self.lon)

    def __reset__(self):
        if not self.gzip:
            self.data_src = open(self.input_filename,'rb')
        else:
            self.data_src = GzipFile(self.input_filename,'rb')

        self.data_src.readline() # header line
        self.is_over = False

    def isOver(self):
        return self.is_over


class Inclusion(DataSource):
    def __init__(self, geonames_filename,hierarchy_filename,mask_ids=None):
        super().__init__("Inclusion SRC",hierarchy_filename)
        assert os.path.exists(geonames_filename)
        self.geonames_data_dict = {row.geonameid:(row.name,row.latitude,row.longitude) for row in read_geonames(geonames_filename).itertuples()}
        
        self.data_src = pd.read_csv(self.input_filename,
            sep="\t",
            header=None,
            names="parentId,childId,type".split(",")
        ).fillna("")
        
        if mask_ids:
            self.data_src = self.data_src[self.data_src.childId.isin(mask_ids)]
        self.data_src= self.data_src[self.data_src.childId.isin(self.geonames_data_dict)]
        self.data_src= self.data_src[self.data_src.parentId.isin(self.geonames_data_dict)]

        self.data_src = self.data_src["childId parentId".split()].values.tolist()
        self.len = len(self.data_src)

        self.i = 0

        self.is_over = False

    def __next__(self):
        if self.i+1 >= self.len:
            self.eof = True
            raise StopIteration
        else:
            self.i += 1
            tup_ = tuple(self.data_src[self.i-1])
            return (self.geonames_data_dict[tup_[0]][0],
            self.geonames_data_dict[tup_[1]][0],
            self.geonames_data_dict[tup_[0]][2],
            self.geonames_data_dict[tup_[0]][1])

    def __reset__(self):
        self.i = 0
        self.is_over = False
    
    def isOver(self):
        return (self.i == self.len)
    



class CoOccurrences(DataSource):
    def __init__(self, filename, label_encoder,sampling=3,resolution = 256,use_healpix=False):
        super().__init__("Co-Occurrence data",filename)
        self.is_there_healpix = use_healpix
        # LOAD DATA

        self.data_src = pd.read_csv(filename,sep="\t")

        # CHECK IF THE HEALPIX RESOLUTION DATA APPEARS IN THE DATA
        if not "healpix_{0}".format(resolution) in self.data_src.columns:
            raise KeyError("healpix_{0} column does not exists ! ".format(resolution))
        
        # PARSE TOPONYMS
        self.data_src["title"] = self.data_src.title.apply(parse_title_wiki)
        try:
            self.data_src["interlinks"] = self.data_src.interlinks.apply(parse_title_wiki)
        except:
            pass

        # LOOP parameter
        self.sampling = sampling
        if self.sampling:
            self.probs_storage = SamplingProbabilities()
            
        # LOOP INDICES
        self.i = 0
        self.j = 0
        self.is_over = False
        self.len = len(self.data_src)*(self.sampling-1)

        
        # BUFFER VARIABLE
        self.topo = None
        self.context_topo_context = []
        self.curr_probs = None
        self.lat, self.lon = None, None


        self.resolution = resolution
        self.classes = self.data_src["healpix_{0}".format(self.resolution)].unique().tolist()

        self.class_encoder = label_encoder
        self.class_encoder.fit(self.classes)

        self.healpix = None

    def __next__(self):
        if self.isOver() or self.i*self.sampling == self.len:
            self.is_over = True
            raise StopIteration 

        if  self.j >= len(self.context_topo_context):
            line = self.data_src.iloc[self.i]
            
            self.topo = line.title
            self.context_topo_context = [x for x in str(line.interlinks).split("|")]
            N = len(self.context_topo_context)
            triple = []
            for i in range(N):
                if i+1 == N:
                    break
                triple.append((self.context_topo_context[i],self.context_topo_context[i+1]))
                

            self.context_topo_context = triple
            np.random.shuffle(self.context_topo_context)
            self.lat, self.lon = line.latitude,line.longitude
            
            self.healpix = line["healpix_{0}".format(self.resolution)]
            
            self.i += 1
            self.j = 0
        
        self.j += 1
        return (self.topo,
        *self.context_topo_context[self.j-1],
        self.lat,self.lon,self.class_encoder.transform([self.healpix])[0])

    def __reset__(self):
        self.i = 0
        self.is_over = False
    
    def isOver(self):
        return self.is_over
    
class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self,data_sources,ngram_index,class_encoder,**kwargs):
        'Initialization'
        self.data_src = data_sources
        self.ngram_index = ngram_index

        self.batch_size = kwargs.get("batch_size",1000)
        self.only_healpix = kwargs.get("only_healpix",False)
        
        self.len = sum([len(d) for d in self.data_src])
        self.datasrc_index = 0

        self.num_classes = class_encoder.get_num_classes()

        self.is_there_healpix = self.data_src[self.datasrc_index].is_there_healpix
    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(self.len / self.batch_size))

    def return_(self,X,y,y2=None):
        if self.is_there_healpix and self.only_healpix:
            return [X[:,0],X[:,1],X[:,2]],y2

        elif self.is_there_healpix:
            return [X[:,0],X[:,1],X[:,2]],[y,y2]
        else:
            return [X[:,0],X[:,1],X[:,2]],y

    def __getitem__(self, index):
        'Generate one batch of data'
        X = np.empty((self.batch_size,3,self.ngram_index.max_len),dtype=np.int32) # toponym
        y = np.empty((self.batch_size,2),dtype=float) #lat lon coord

        y2=None # For healpix
        if self.is_there_healpix:
            y2 = np.empty((self.batch_size,self.num_classes),dtype=float) # healpix class

        if self.data_src[self.datasrc_index].isOver():
                self.datasrc_index += 1
                self.is_there_healpix = self.data_src[self.datasrc_index].is_there_healpix

        if self.datasrc_index >= len(self.data_src):
            self.return_(X,y,y2)
        
        for i in range(self.batch_size):
            if self.data_src[self.datasrc_index].isOver():
                return self.return_(X,y,y2)
            try:
                topo, topo_context_1,topo_context_2, latitude, longitude, healpix_class = self.data_src[self.datasrc_index].__next__()
            except StopIteration as e:
                return self.return_(X,y,y2)
            
            X[i] = [ self.ngram_index.encode(topo),self.ngram_index.encode(topo_context_1),self.ngram_index.encode(topo_context_2)]
            y[i] =  [*zero_one_encoding(longitude,latitude)]
            if self.is_there_healpix:
                y2[i] = to_categorical(healpix_class, num_classes=self.num_classes, dtype='int32'
)

            #y[i] = [longitude,latitude]
        return self.return_(X,y,y2)

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        [d.__reset__() for d in self.data_src]
        self.datasrc_index = 0


    
def load_embedding(model_fn,dim_vector=100):
    model = KeyedVectors.load(model_fn)
    N = len(model.wv.vocab)
    M = np.zeros((N,dim_vector))
    for i in range(N):
        try:
            M[i] = model.wv[str(i)]
        except KeyError:
            pass
    return M

if __name__ == "__main__":
    # All adj nb of line :7955000-1
    from lib.ngram_index import NgramIndex
    from tqdm import tqdm
    ng = NgramIndex.load("../data/embeddings/word2vec4gram/4gramWiki+geonames_index.json")
    c= CoOccurrences("../data/wikipedia/cooccurrence_FR.txt_test.csv",sampling=3)
    a = Adjacency("/home/jacques/sample_adjacency.txt",geonames_filename="../data/geonamesData/allCountries.txt",gzip=False,sampling=10)
    i= Inclusion(geonames_filename="../data/geonamesData/allCountries.txt",hierarchy_filename="../data/geonamesData/hierarchy.txt")
    d= DataGenerator([c,a,i],ng) 
    for x in tqdm(range(len(d))):d[i]
