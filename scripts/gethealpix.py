

import pandas as pd

from tqdm import tqdm
tqdm.pandas()
import argparse

import numpy as np
import healpy
# convert lat and lon to a healpix code encoding a region, with a given resolution
def latlon2healpix( lat , lon , res ):
    lat = np.radians(lat)
    lon = np.radians(lon)
    xs = ( np.cos(lat) * np.cos(lon) )#
    ys = ( np.cos(lat) * np.sin(lon) )# -> Sphere coordinates: https://vvvv.org/blog/polar-spherical-and-geographic-coordinates
    zs = ( np.sin(lat) )#
    return healpy.vec2pix( int(res) , xs , ys , zs )

parser = argparse.ArgumentParser()
parser.add_argument("input_file")
parser.add_argument("output_file")

args = parser.parse_args()

df = pd.read_csv(args.input_file,sep="\t") 
df["healpix_256"] = df.progress_apply(lambda row:latlon2healpix(lat=row.latitude,lon=row.longitude,res=256),axis=1)
df["healpix_64"] = df.progress_apply(lambda row:latlon2healpix(lat=row.latitude,lon=row.longitude,res=64),axis=1)
df["healpix_32"] = df.progress_apply(lambda row:latlon2healpix(lat=row.latitude,lon=row.longitude,res=32),axis=1)
df["healpix_1"] = df.progress_apply(lambda row:latlon2healpix(lat=row.latitude,lon=row.longitude,res=1),axis=1)

df.to_csv(args.output_file,sep="\t",index=False)