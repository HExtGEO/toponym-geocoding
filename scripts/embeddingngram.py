#!/usr/bin/env python
# coding: utf-8


from lib.ngram_index import NgramIndex
from lib.utils_geo import read_geonames



import pandas as pd
import numpy as np
from tqdm import tqdm


from tqdm import tqdm



from gensim.models import Word2Vec
import logging
logging.basicConfig(level="INFO")



df_cooc = pd.read_csv("../data/wikipedia/cooccurrence_ALL.txt",sep="\t")
df_geo = read_geonames("../data/geonamesData/allCountries.txt") 


geonames_label = df_geo.name.values.tolist()
wiki_labels = df_cooc.title.values.tolist()
p= [wiki_labels.extend(x.split("|")) for x in df_cooc["interlinks"].values]


del df_geo
del df_cooc

N = 5


ng = NgramIndex(N)
p = [ng.split_and_add(x) for x in tqdm(geonames_label)]
p = [ng.split_and_add(x) for x in tqdm(wiki_labels)]
ng.save("{0}gramWiki+Geonames_index.json".format(N))

geonames_label.extend(wiki_labels)

class MySentences(object):
    def __init__(self, texts):
        self.texts = texts

    def __iter__(self):
        for w in self.texts:
            yield [str(x)for x in ng.encode(w)]

model = Word2Vec(MySentences(geonames_label), size=100, window=5, min_count=1, workers=4,sg=1)
model.save("embedding{0}gramWiki+Geonames.bin".format(5))



