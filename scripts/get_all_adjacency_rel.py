import pandas as pd, numpy as np
from numba import njit
from helpers import read_geonames
from tqdm import tqdm
from joblib import Parallel,delayed
import geopandas as gpd
from lib.utils_geo import Grid,haversine_pd
import matplotlib.pyplot as plt

import argparse

parser = argparse.ArgumentParser()

parser.add_argument("geoname_fn")
parser.add_argument("kilometer_threshold",type=int,default=20)
parser.add_argument("output_fn_prefix")

args = parser.parse_args("../data/geonamesData/allCountries.txt 20 /home/jacques/ALL_ADJ_224+_".split())

GEONAME_FN = args.geoname_fn
PREFIX_OUTPUT_FN = args.output_fn_prefix
KM_THRESHOLD = args.kilometer_threshold

df = read_geonames(GEONAME_FN)

def to_str(list_):
    """
    Return str representation for each value in list_
    
    Parameters
    ----------
    list_ : array
        array
    
    Returns
    -------
    array
        str list
    """
    return list(map(str,list_))

def get_adjacent(geonameid,ids,lon1, lat1, lon2, lat2,threshold):
    """
    Write adjacent entry in geonames for a selected entry
    """
    dist_ = haversine_pd(lon1, lat1, lon2, lat2)
    adj_ids = ids[dist_<threshold]
    out_.write("\n{0},{1},{2},{3}".format(geonameid,"|".join(to_str(adj_ids)),lat2,lon2))
    out_.flush()


# WE BUILD a grid over the world map
# It allows to limit unnecessary calculus thus accelerate the whole process
world = gpd.read_file("/media/jacques/DATA/GEODATA/WORLD/world.geo.50m.dissolved")
g = Grid(*world.bounds.values[0],[40,20]) #We build a grid of cell of 40° by 20°
g.fit_data(world)

# Prepare first output
first_output_fn = "{1}{0}_cells.csv".format(KM_THRESHOLD,PREFIX_OUTPUT_FN)
out_ = open(first_output_fn,'w')
out_.write("geonameid,adjacent_geonameid,latitude,longitude") # HEADER
out_.flush() # Avoid writing bugs

def get_rels(cells_list):
    for c in tqdm(cells_list):
        
        mask1 = (df.latitude <= c.bottomright_y) & (df.latitude >= c.upperleft_y)
        new_df = df[mask1].copy() 
        mask2 = (new_df.longitude >= c.upperleft_x) & (new_df.longitude <= c.bottomright_x)
        new_df = new_df[mask2]
        for ix,row in new_df.iterrows():
            get_adjacent(row.geonameid,new_df.geonameid.values,new_df.longitude,new_df.latitude,row.longitude,row.latitude,KM_THRESHOLD)
        #Parallel(n_jobs=-1,backend="multiprocessing",temp_folder="/home/jacques/temp/")(delayed(get_adjacent)(row.geonameid,new_df.geonameid.values,new_df.longitude,new_df.latitude,row.longitude,row.latitude,KM_THRESHOLD) for ix,row in new_df.iterrows())

world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
ax = world.plot(color="white",edgecolor="black")
for c in g.cells[224:]:
    ax.plot(*c.box_.exterior.xy)
plt.show()
get_rels(g.cells[224:]) #~3h

# Prepare second output
# second_output_fn = "{1}{0}_inter_cells.csv".format(KM_THRESHOLD,PREFIX_OUTPUT_FN)
# out_ = open(second_output_fn,'w')
# out_.write("geonameid,adjacent_geonameid,latitude,longitude") # HEADER
# out_.flush()# Avoid writing bugs

# get_rels(g.inter_cells) 594
