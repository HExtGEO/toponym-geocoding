import pandas as pd 
import re

#### TODO NEED TO add ARGPARSE !!!
def parse_title_wiki(title_wiki):
    """
    Parse Wikipedia title
    
    Parameters
    ----------
    title_wiki : str
        wikipedia title
    
    Returns
    -------
    str
        parsed wikipedia title
    """
    return re.sub("\(.*\)", "", title_wiki).strip().lower()


df = pd.read_csv("./cooccurrence_US_FR.txt",sep="\t")

df["interlinks"] = df.interlinks.apply(lambda x : x.split("|"))
df["interlinks"] = df.interlinks.apply(lambda x : [parse_title_wiki(i) for i in x])

df["title"] = df.title.apply(parse_title_wiki)

def generated_inputs(x):
    output = []
    for interlink in x.interlinks:
        output.append([x.title,interlink,x.longitude,x.latitude])
    return output

output_ =  []
for ix,row in df.iterrows():
    output_.extend(generated_inputs(row))

new_df = pd.DataFrame(output_,columns="name1 name2 longitude latitude".split())  
new_df = new_df.sample(frac=1)
new_df.to_csv("us_fr_cooc_test.csv",index=False)