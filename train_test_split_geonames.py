import argparse

import numpy as np
import pandas as pd
import geopandas as gpd

import logging
logging.basicConfig(
    format='[%(asctime)s][%(levelname)s] %(message)s ', 
    datefmt='%m/%d/%Y %I:%M:%S %p',
    level=logging.INFO
    )

from sklearn.model_selection import train_test_split

from lib.utils_geo import latlon2healpix
from helpers import read_geonames

from tqdm import tqdm 

parser = argparse.ArgumentParser()
parser.add_argument("geoname_file")
parser.add_argument("--feature_classes",help="List of class",default="A P")

args = parser.parse_args()#("data/geonamesData/FR.txt".split())

# LOAD DATAgeopandas
GEONAME_FN = args.geoname_file
FEATURE_CLASSES = args.feature_classes


logging.info("Load Geonames data...")
geoname_data = read_geonames(GEONAME_FN).fillna("")
logging.info("Geonames data loaded!")

# SELECT ENTRY with class == to A and P (Areas and Populated Places)
filtered = geoname_data[geoname_data.feature_class.isin(FEATURE_CLASSES.split())].copy() # Only take area and populated places

filtered["cat"] = filtered.apply(lambda x:latlon2healpix(x.latitude,x.longitude,64),axis=1)
# TRAIN AND TEST SPLIT
logging.info("Split Between Train and Test")

#  Cell can be empty
cat_unique = filtered.cat.unique()
ci=0
while 1:
    if len(filtered[filtered.cat == cat_unique[ci]])> 1:
        X_train,X_test = train_test_split(filtered[filtered.cat == cat_unique[ci]])
        break
    ci+=1

for i in cat_unique[ci:] :
    try:
        x_train,x_test = train_test_split(filtered[filtered.cat == i])
        X_train,X_test = pd.concat((X_train,x_train)),pd.concat((X_test,x_test))
    except:
        pass #print("Error",len(filtered[filtered.cat == i]))


del X_train["cat"]
del X_test["cat"]

# SAVING THE DATA
logging.info("Saving Output !")
X_train.to_csv(GEONAME_FN+"_train.csv")
X_test.to_csv(GEONAME_FN+"_test.csv")